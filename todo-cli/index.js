/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const { sequelize, connect } = require("./connectDB");
const Todo = require("./TodoModel");

const createTodo = async () => {
  try {
    await connect();

    const todo = await Todo.addTask({
      title: "Third Item",
      dueDate: new Date(),
      completed: false,
    });
    console.log(`Created todo with ID : ${todo.id}`);
  } catch (error) {
    console.error(error);
  }
};

const countItem = async () => {
  try {
    const totalItem = await Todo.count();
    console.log(`Found ${totalItem} items in the Table i.e rows   `);
  } catch (error) {
    console.error(error);
  }
};

const getllTodos = async () => {
  try {
    const todos = await Todo.findAll({
      order: [["id", "ASC"]],
    });

    const todoList = todos.map((todo) => todo.displayableString()).join("\n");
    console.log(todoList);
  } catch (error) {
    console.error(error);
  }
};

const getSingleTodo = async () => {
  try {
    const todo = await Todo.findOne({
      where: {
        completed: false,
      },
      order: [["id", "ASC"]],
    });

    console.log(todo.displayableString());
  } catch (error) {
    console.error(error);
  }
};

const updateItem = async (id) => {
  try {
    await Todo.update(
      { title: "Second Item" },
      {
        where: {
          id: id,
        },
      }
    );
  } catch (error) {
    console.error(error);
  }
};

const deleteItem = async (id) => {
  try {
    const deletedRowCount = await Todo.destroy({
      where: {
        id: id,
      },
    });

    console.log(`Deleted ${deletedRowCount} rows! `);
  } catch (error) {
    console.error(error);
  }
};

(async () => {
  await updateItem(2);

  await getllTodos();

  await deleteItem(3);

  await getllTodos();

  await getllTodos();

  await getSingleTodo();

  await countItem();
})();
